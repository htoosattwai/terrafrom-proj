resource "aws_security_group" "web-sg" {
  name        = "web-sg"
  description = "Allow SSH & 8080"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH from Public"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  ingress {
    description = "8080 from Public"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh_8080"
  }
}

data "aws_ami" "amazon2" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_name]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # amazon
}

resource "aws_instance" "web" {
  ami                         = data.aws_ami.amazon2.id
  instance_type               = var.instance_type
  availability_zone = var.public_subnet_availability_zone
  subnet_id                   = var.pub_subnet_id
  associate_public_ip_address = true
  key_name                    = var.ssh_key
  vpc_security_group_ids      = [aws_security_group.web-sg.id]
  tags = {
    Name = "${var.prefix}-Nginx-A"
  }
}

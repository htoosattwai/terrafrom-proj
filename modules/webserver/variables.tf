variable "vpc_id" {}
variable "public_subnet_availability_zone" {}
variable "pub_subnet_id" {}
variable "ssh_key" {}
variable "ami_name" {}
variable "instance_type" {}
variable "prefix" {}
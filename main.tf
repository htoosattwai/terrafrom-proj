provider "aws" {
  region     = var.region
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "dev-vpc"
  cidr = var.vpc_cidr_block

  azs             = var.public_azs
  private_subnets = var.private_subnet_cidr
  public_subnets  = var.public_subnet_cidr
  public_subnet_tags = {
    name = "${var.prefix}-public_subnet"
  }
  tags = {
    name = "${var.prefix}-vpc"
  }
}

module "web-instances" {
  source                          = "./modules/webserver"
  vpc_id                          = module.vpc.vpc_id
  ami_name                        = var.ami_name
  pub_subnet_id                   = module.vpc.public_subnets[0]
  instance_type                   = var.instance_type
  public_subnet_availability_zone = module.vpc.azs[0]
  ssh_key                         = var.ssh_key
  prefix                          = var.prefix

}
variable "region" {}
variable "prefix" {}
variable "vpc_cidr_block" {}
variable "instance_type" {}
variable "public_azs" {
  description = "List of Public AZs"
  type        = list(any)
}
variable "private_azs" {
  description = "List of Private AZs"
  type        = list(any)
}
variable "public_subnet_cidr" {
  description = "List of Public Subnets"
  type        = list(any)
}
variable "private_subnet_cidr" {
  description = "List of Private Sunets"
  type        = list(any)
}
variable "ssh_key" {}
variable "ami_name" {}
#variable "default_route_table_id" {}  
variable "raymond_ssh_key" {
  sensitive = true
}
